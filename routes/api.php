<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'AuthController@register');
//Route::post('phone/send-verification', 'AuthController@sendVerificationCode');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::post('phone/verify-code', 'AuthController@verifyCode');
    Route::get('logout', 'AuthController@logout');

    Route::get('test', function(Request $request){
        $input = $request->all();
        $user = DB::table('users')->select('name', 'email')->where('token', $input['token'])->first();
        print_r($user);
        return response()->json(['foo'=>'bar']);
    });
});